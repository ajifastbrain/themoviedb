//
//  Strings.swift
//  TheMovieDB
//
//  Created by Aji Prakosa on 01/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct Localization {
    struct Label {
        static let CATEGORY = "Category".localized
        static let SOMETHING_WENT_WRONG = "Something Went Wrong".localized
    }
}
