//
//  Connectivity.swift
//  TheMovieDB
//
//  Created by Aji Prakosa on 01/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
